# ScreenRain

**https://www.roblox.com/library/1197517532/ScreenRain**

*The following is inside the code as well, but for redundancy and ease of use, it is here as well.*

**AUTHOR:** Zack

**CONTACT INFO:** @BoatbomberRBLX on Twitter, boatbomber on Roblox

**CREATION DATE:** 12/10/2017

**PURPOSE:** Creates a realistic on-screen rain effect for games

**SPECIAL NOTES:**	Place inside a local domain. List of possible domains:
	*ReplicatedFirst, StarterGui, StarterPack, StarterPlayer.StarterPlayerScripts, or StarterPlayer.StarterCharacterScripts*
	
# API

**Settings:**

		Distortion:
			Description: 		Toggles if the rain droplets are made of glass, causing realistic "water" distortion. If false, uses ImageLabels instead.
			Type: 				Boolean
 			Example: 			true

 		Rate:
 			Description: 		How many droplets spawn per second
			Type: 				Number
 			Example: 			5
			Size:
 				Description: 		How large the droplets are. If using GUIs, set in pixels; if using Distortion, set in studs.
				Type: 				Number
	 			Example: 			40 (for pixels) 0.3 (for studs)

 		SizeRandomness:
 			Description:		Toggles if the droplets are exactly sized, or can be a little off (for a more natural feeling)
			Type: 				Boolean
 			Example: 			true

 			SizeRandomNoise:
 				Description: 	Sets how far off the size can be. If using GUIs, set in pixels; if using Distortion, set in studs. (Only matters if SizeRandomness is true)
				Type: 			Number
 				Example: 		10 (for pixels) 0.1 (for studs)

 		Tint:
 			Description:		Toggles if the droplets are tinted. If false, it will be a realistic blue-ish clear
			Type: 				Boolean
 			Example: 			false

 			TintColor:
 				Description:	Sets the color of the tint (Only matters if Tint is true)
				Type:			Color3
 				Example:		Color3.fromRGB(117, 230, 255)
 
 		FadeSpeed:
 			Description:		How quickly the droplets fade away, in seconds.
			Type:				Number
			Example:			1

 		Sound:
 			Description:		Toggles dynamic sound. (Sound lowers when indoors, volume changes based on camera angle, etc.)
			Type:				Boolean
 			Example:			true

 		Theme:
 			Description:		If Distortion is false, this can be set to "Preset" or "Custom" to either use the preset droplet images, or use your own.
			Type:				String
 			Example:			"Preset"

			Themes:
 				Description: 	Table with the Preset theme, and a place to create your Custom theme
				Type:			Table
				Example:		Preset = {1197357237, 1197356853, 1197357004, 1197356610, 1197356458, 1197357445, 1197357656, 1197358187, 1197358487, 1197357888};
								Custom = {}

 		PhysicalRain:
 			Description:		Toggles part rain falling in the world around the player. (Done locally, so it doesn't lag nor affect other players)
			Type:				Boolean
 			Example:			true

 			PhysicalRainWind:
 				Description: 	Adds a wind effect to the rain, in X and Z axis of world space. It's set by angles, so 50 is a strong wind, etc. (Only matters if PhysicalRain is true)
				Type: 			Vector2
 				Example: 		Vector2.new(5,5)

 			PhysicalRainRadius:
 				Description: 	How far around the player the rain will fall. (Only matters if PhysicalRain is true)
				Type: 			Number
 				Example: 		50
				
 			Gusts:		--[[DO NOT RECOMMEND USING YET. CAUSES MAJOR FRAME DROPS]]--
				Description: 	Allows the wind to flare up for a moment, rather than only be constant.
				Type: 			Boolean
				Example: 		false

				GustsPerMin:
					Description: 	How many gusts will occur per minute, on average. (Only matters if Gusts is true)
					Type: 			Number
 					Example: 		20

				GustsStrength:
					Description: 	How much extra wind a gust can add. (Only matters if Gusts is true)
					Type: 			Number
 					Example: 		10